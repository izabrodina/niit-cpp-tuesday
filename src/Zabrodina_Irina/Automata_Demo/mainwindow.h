#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "automata.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:


private slots:

    void start();

    void timeout();

    void startCancel();

    void timeoutCancel();

    void on_pbDrink1_clicked();

    void on_pbDrink2_clicked();

    void on_pbDrink3_clicked();

    void on_pbDrink4_clicked();

    void on_pbDrink5_clicked();

    void on_pbDrink6_clicked();

    void on_pbDrink7_clicked();

    void on_pb_1_clicked();

    void on_pb_2_clicked();

    void on_pb_5_clicked();

    void on_pb_10_clicked();

    void on_pb_50_clicked();

    void on_pb_100_clicked();

    void on_pbCook_clicked();

    void on_pbCancel_clicked();

private:
    Ui::MainWindow *ui;
    Automata * automata;
    QTimer * qTimer;
    QTimer * qCancel;
    int progressValue;
    int cancelValue;
};

#endif // MAINWINDOW_H
