#include "automata.h"

Automata::Automata()
{
    currentState = OFF;
    cash = 0.0;
    total = 0.0;
    change = 0.0;
}

void Automata::on(){
    currentState = WAIT;
}

void Automata::off(){
    currentState = OFF;
    cash = 0.0;
}

void Automata::coin(float money){
    currentState = ACCEPT;
    cash += money;
}

float Automata::getChange() const{
    return change;
}

void Automata::parseMenu(){
    QFile * file = new QFile("C:\\Users\\Irene\\Desktop\\Programming\\Qt\\NIIT\\Automata_Demo\\coffee_machine.xml");
    if (!file->open(QIODevice::ReadOnly|QIODevice::Text)){
        QMessageBox message;
        message.setText("File error!");
        message.exec();
        exit(1);
    }

    QXmlStreamReader xml(file);
    QXmlStreamReader::TokenType token;

    while (!xml.atEnd() && !xml.hasError()){
        token = xml.readNext();
        if (token == QXmlStreamReader::StartDocument)
            continue;

        if (token == QXmlStreamReader::StartElement){
            if (xml.name() == "drinks")
                continue;
            if (xml.name() == "drink"){
                QString drink;
                int price;
                QXmlStreamAttributes attribute = xml.attributes();
                /*
                   if (attribute.hasAttribute("id"))
                   id = attribute.value("id").toString();
                */
                if (attribute.hasAttribute("drink"))
                    drink = attribute.value("drink").toString();
                if (attribute.hasAttribute("price"))
                    price = attribute.value("price").toInt();
                menu.insert(std::make_pair(drink, price));
            }
        }
    }
}

void Automata::printMenu(std::map <QString, int> menu, QPushButton **buttons){
    for (int i = 0; i < 8; i++){
        if (!menu.empty()){
            buttons[i]->setText(menu.begin()->first + " " + QString::number(menu.begin()->second));
            menu.erase(menu.begin());
        }
    }
}

std::map <QString, int> Automata::getMenu() const {
    return menu;
}

void Automata::choice(float price){
    total = price;
    check(price);
}

bool Automata::check(float price){
    if (cash < price){
        currentState = FAILED;
        change = cash;
        return false;
    }
    else {
        currentState = CHECKED;
        change = cash - price;
        return true;
    }
}

void Automata::cancel(){
    currentState = WAIT;
    change = cash;
    cash = 0.0;
}

void Automata::cook(){
    currentState = COOK;
}

void Automata::finish(){
    currentState = READY;
    total += cash;
    cash = 0.0;
}

STATES Automata::getCurrentState() const{
    return currentState;
}

QString Automata::showNavigationText() const{

    switch (currentState){
    case WAIT:
        return QString("Welcome!\nPlease choose a drink and put some money");
    case ACCEPT:
        return QString("You have ") + QString::number(cash);
    case COOK:
        return QString("Cooking your drink...\nPlease wait");
    case CHECKED:
        return QString("Good choice!\nPlease press \"Cook\" and we'll make this drink for you.\n");
    case FAILED:
        return QString("Not enough money. Please add some cash or cancel transaction.");
    case READY:
        return QString("Ready. Please take your drink and change ");
    }
}
