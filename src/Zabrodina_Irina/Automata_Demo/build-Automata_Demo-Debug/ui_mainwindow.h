/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *pb_100;
    QPushButton *pb_50;
    QPushButton *pb_10;
    QPushButton *pb_5;
    QPushButton *pb_1;
    QPushButton *pb_2;
    QProgressBar *progressBar;
    QPushButton *pbCook;
    QPushButton *pbCancel;
    QTextEdit *navigationText;
    QPushButton *pbDrink1;
    QPushButton *pbDrink2;
    QPushButton *pbDrink3;
    QPushButton *pbDrink4;
    QPushButton *pbDrink5;
    QPushButton *pbDrink6;
    QPushButton *pbDrink7;
    QLabel *labelPic;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(557, 699);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pb_100 = new QPushButton(centralWidget);
        pb_100->setObjectName(QStringLiteral("pb_100"));
        pb_100->setGeometry(QRect(20, 290, 93, 28));
        pb_50 = new QPushButton(centralWidget);
        pb_50->setObjectName(QStringLiteral("pb_50"));
        pb_50->setGeometry(QRect(120, 290, 93, 28));
        pb_10 = new QPushButton(centralWidget);
        pb_10->setObjectName(QStringLiteral("pb_10"));
        pb_10->setGeometry(QRect(220, 290, 93, 28));
        pb_5 = new QPushButton(centralWidget);
        pb_5->setObjectName(QStringLiteral("pb_5"));
        pb_5->setGeometry(QRect(20, 330, 93, 28));
        pb_1 = new QPushButton(centralWidget);
        pb_1->setObjectName(QStringLiteral("pb_1"));
        pb_1->setGeometry(QRect(220, 330, 93, 28));
        pb_2 = new QPushButton(centralWidget);
        pb_2->setObjectName(QStringLiteral("pb_2"));
        pb_2->setGeometry(QRect(120, 330, 93, 28));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(50, 200, 251, 23));
        progressBar->setValue(24);
        pbCook = new QPushButton(centralWidget);
        pbCook->setObjectName(QStringLiteral("pbCook"));
        pbCook->setGeometry(QRect(50, 240, 93, 28));
        pbCancel = new QPushButton(centralWidget);
        pbCancel->setObjectName(QStringLiteral("pbCancel"));
        pbCancel->setGeometry(QRect(170, 240, 93, 28));
        navigationText = new QTextEdit(centralWidget);
        navigationText->setObjectName(QStringLiteral("navigationText"));
        navigationText->setEnabled(false);
        navigationText->setGeometry(QRect(20, 10, 291, 171));
        QFont font;
        font.setPointSize(12);
        navigationText->setFont(font);
        pbDrink1 = new QPushButton(centralWidget);
        pbDrink1->setObjectName(QStringLiteral("pbDrink1"));
        pbDrink1->setGeometry(QRect(340, 10, 181, 41));
        pbDrink2 = new QPushButton(centralWidget);
        pbDrink2->setObjectName(QStringLiteral("pbDrink2"));
        pbDrink2->setGeometry(QRect(340, 60, 181, 41));
        pbDrink3 = new QPushButton(centralWidget);
        pbDrink3->setObjectName(QStringLiteral("pbDrink3"));
        pbDrink3->setGeometry(QRect(340, 110, 181, 41));
        pbDrink4 = new QPushButton(centralWidget);
        pbDrink4->setObjectName(QStringLiteral("pbDrink4"));
        pbDrink4->setGeometry(QRect(340, 160, 181, 41));
        pbDrink5 = new QPushButton(centralWidget);
        pbDrink5->setObjectName(QStringLiteral("pbDrink5"));
        pbDrink5->setGeometry(QRect(340, 210, 181, 41));
        pbDrink6 = new QPushButton(centralWidget);
        pbDrink6->setObjectName(QStringLiteral("pbDrink6"));
        pbDrink6->setGeometry(QRect(340, 260, 181, 41));
        pbDrink7 = new QPushButton(centralWidget);
        pbDrink7->setObjectName(QStringLiteral("pbDrink7"));
        pbDrink7->setGeometry(QRect(340, 310, 181, 41));
        labelPic = new QLabel(centralWidget);
        labelPic->setObjectName(QStringLiteral("labelPic"));
        labelPic->setGeometry(QRect(30, 370, 491, 261));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 557, 26));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        pb_100->setText(QApplication::translate("MainWindow", "100", 0));
        pb_50->setText(QApplication::translate("MainWindow", "50", 0));
        pb_10->setText(QApplication::translate("MainWindow", "10", 0));
        pb_5->setText(QApplication::translate("MainWindow", "5", 0));
        pb_1->setText(QApplication::translate("MainWindow", "1", 0));
        pb_2->setText(QApplication::translate("MainWindow", "2", 0));
        pbCook->setText(QApplication::translate("MainWindow", "Cook", 0));
        pbCancel->setText(QApplication::translate("MainWindow", "Cancel", 0));
        pbDrink1->setText(QString());
        pbDrink2->setText(QString());
        pbDrink3->setText(QString());
        pbDrink4->setText(QString());
        pbDrink5->setText(QString());
        pbDrink6->setText(QString());
        pbDrink7->setText(QString());
        labelPic->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
