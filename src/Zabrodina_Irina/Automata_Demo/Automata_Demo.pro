#-------------------------------------------------
#
# Project created by QtCreator 2016-05-23T16:14:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Automata_Demo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    automata.cpp

HEADERS  += mainwindow.h \
    automata.h

FORMS    += mainwindow.ui
