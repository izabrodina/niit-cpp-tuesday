#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    automata = new Automata;
    qTimer = new QTimer;
    qCancel = new QTimer;
    progressValue = 0;
    cancelValue = 0;
    ui->progressBar->setValue(progressValue);
    automata->on();
    ui->navigationText->setText(automata->showNavigationText());
    QPushButton *buttons[] = {ui->pbDrink1, ui->pbDrink2, ui->pbDrink3,
                              ui->pbDrink4, ui->pbDrink5, ui->pbDrink6,
                              ui->pbDrink7};
    automata->parseMenu();
    automata->printMenu(automata->getMenu(), buttons);
    ui->pbCook->setEnabled(false);
    ui->pbCancel->setEnabled(false);

    QPixmap coffeePicture("C:\\Users\\Irene\\Desktop\\Programming\\Qt\\NIIT\\Automata_Demo\\coffee.jpg");
    ui->labelPic->setPixmap(coffeePicture);

    connect(ui->pbCook, SIGNAL(clicked()), this, SLOT(start()));
    connect(this->qTimer, SIGNAL(timeout()), this, SLOT(timeout()));

    connect(ui->pbCancel, SIGNAL(clicked()), this, SLOT(startCancel()));
    connect(this->qCancel, SIGNAL(timeout()), this, SLOT(timeoutCancel()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbCancel_clicked()
{
    automata->cancel();
    ui->navigationText->setText("Transaction canceled.\nYour change: " + QString::number(automata->getChange()));
    ui->pbCancel->setEnabled(false);
}

void MainWindow::start()
{
    qTimer->setInterval(50);
    qTimer->start();
}

void MainWindow::timeout()
{
    ui->pbCook->setEnabled(false);
    ui->pbCancel->setEnabled(false);
    if(progressValue <= 100){
        ui->progressBar->setValue(progressValue++);
    }
    else if(progressValue++ < 300)
    {
        automata->finish();
        ui->navigationText->setText(automata->showNavigationText() + QString::number(automata->getChange()) + ".\nHave a nice day!");
        this->update();
    }
    else
    {
        qTimer->stop();
        this->update();
        progressValue = 0;
        automata->on();
        ui->navigationText->setText(automata->showNavigationText());
    }
}

void MainWindow::startCancel()
{
    qCancel->setInterval(50);
    qCancel->start();
}

void MainWindow::timeoutCancel()
{
    if(cancelValue++ <= 100){
        ui->navigationText->setText("Transaction canceled.\nYour change: " + QString::number(automata->getChange()));
        this->update();
    }
    else
    {
        qCancel->stop();
        this->update();
        cancelValue = 0;
        automata->on();
        ui->navigationText->setText(automata->showNavigationText());
    }
}
//choosing a drink
void MainWindow::on_pbDrink1_clicked()
{
    ui->pbCancel->setEnabled(true);
    QStringList drinkAndItsPrice = ui->pbDrink1->text().split(" ", QString::SkipEmptyParts);
    automata->choice(drinkAndItsPrice[1].toFloat());
    if (automata->getCurrentState() == CHECKED){
        ui->pbCook->setEnabled(true);
    }
    ui->navigationText->setText(automata->showNavigationText());
}

void MainWindow::on_pbDrink2_clicked()
{
    ui->pbCancel->setEnabled(true);
    QStringList drinkAndItsPrice = ui->pbDrink2->text().split(" ", QString::SkipEmptyParts);
    automata->choice(drinkAndItsPrice[1].toFloat());
    if (automata->getCurrentState() == CHECKED){
        ui->pbCook->setEnabled(true);
    }
    ui->navigationText->setText(automata->showNavigationText());
}

void MainWindow::on_pbDrink3_clicked()
{
    ui->pbCancel->setEnabled(true);
    QStringList drinkAndItsPrice = ui->pbDrink3->text().split(" ", QString::SkipEmptyParts);
    automata->choice(drinkAndItsPrice[1].toFloat());
    if (automata->getCurrentState() == CHECKED){
        ui->pbCook->setEnabled(true);
    }
    ui->navigationText->setText(automata->showNavigationText());
}

void MainWindow::on_pbDrink4_clicked()
{
    ui->pbCancel->setEnabled(true);
    QStringList drinkAndItsPrice = ui->pbDrink4->text().split(" ", QString::SkipEmptyParts);
    automata->choice(drinkAndItsPrice[1].toFloat());
    if (automata->getCurrentState() == CHECKED){
        ui->pbCook->setEnabled(true);
    }
    ui->navigationText->setText(automata->showNavigationText());
}

void MainWindow::on_pbDrink5_clicked()
{
    ui->pbCancel->setEnabled(true);
    QStringList drinkAndItsPrice = ui->pbDrink5->text().split(" ", QString::SkipEmptyParts);
    automata->choice(drinkAndItsPrice[1].toFloat());
    if (automata->getCurrentState() == CHECKED){
        ui->pbCook->setEnabled(true);
    }
    ui->navigationText->setText(automata->showNavigationText());
}

void MainWindow::on_pbDrink6_clicked()
{
    ui->pbCancel->setEnabled(true);
    QStringList drinkAndItsPrice = ui->pbDrink6->text().split(" ", QString::SkipEmptyParts);
    automata->choice(drinkAndItsPrice[1].toFloat());
    if (automata->getCurrentState() == CHECKED){
        ui->pbCook->setEnabled(true);
    }
    ui->navigationText->setText(automata->showNavigationText());
}

void MainWindow::on_pbDrink7_clicked()
{
    ui->pbCancel->setEnabled(true);
    QStringList drinkAndItsPrice = ui->pbDrink7->text().split(" ", QString::SkipEmptyParts);
    automata->choice(drinkAndItsPrice[1].toFloat());
    if (automata->getCurrentState() == CHECKED){
        ui->pbCook->setEnabled(true);
    }
    ui->navigationText->setText(automata->showNavigationText());
}

//adding some cash
void MainWindow::on_pb_1_clicked()
{
    automata->coin(1.0f);
    ui->navigationText->setText(automata->showNavigationText());
    ui->pbCancel->setEnabled(true);
}

void MainWindow::on_pb_2_clicked()
{
    automata->coin(2.0f);
    ui->navigationText->setText(automata->showNavigationText());
    ui->pbCancel->setEnabled(true);
}

void MainWindow::on_pb_5_clicked()
{
    automata->coin(5.0f);
    ui->navigationText->setText(automata->showNavigationText());
    ui->pbCancel->setEnabled(true);
}

void MainWindow::on_pb_10_clicked()
{
    automata->coin(10.0f);
    ui->navigationText->setText(automata->showNavigationText());
    ui->pbCancel->setEnabled(true);
}

void MainWindow::on_pb_50_clicked()
{
    automata->coin(50.0f);
    ui->navigationText->setText(automata->showNavigationText());
    ui->pbCancel->setEnabled(true);
}

void MainWindow::on_pb_100_clicked()
{
    automata->coin(100.0f);
    ui->navigationText->setText(automata->showNavigationText());
    ui->pbCancel->setEnabled(true);
}

void MainWindow::on_pbCook_clicked()
{
    automata->cook();
    ui->navigationText->setText(automata->showNavigationText());
}
