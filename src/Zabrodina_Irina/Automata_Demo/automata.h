#ifndef AUTOMATA_H
#define AUTOMATA_H

#include <QMap>
#include <QMessageBox>
#include <QPushButton>
#include <QFile>
#include <QXmlStreamReader>

enum STATES {OFF, WAIT, ACCEPT, CHECKED, FAILED, COOK, READY};

class Automata
{
public:
    Automata();
    void on();
    void off();
    void coin(float money);
    float getChange() const;
    void parseMenu();
    void printMenu(std::map <QString, int> menu, QPushButton **arr);
    std::map <QString, int> getMenu() const;
    void printState();
    void choice(float price);
    bool check(float price);
    void cancel();
    void cook();
    void finish();
    STATES getCurrentState() const;
    QString showNavigationText() const;

private:
    float cash;
    float total;
    float change;
    std::map <QString, int> menu;
    STATES currentState;

};

#endif // AUTOMATA_H
