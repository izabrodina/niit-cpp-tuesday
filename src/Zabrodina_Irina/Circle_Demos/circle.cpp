#include "circle.h"

const double pi = 3.14159;

Circle::Circle()
:radius(0), ference(0), area(0)
{}
void Circle::setRadius(double r)
{
    if (r < 0) throw "Radius cannot be negative. Try again.";
    radius = r;
    ference = 2 * pi * r;
    area = pi * r * r;
}

void Circle::setFerence(double f)
{
    if (f < 0) throw "Ference cannot be negative. Try again.";
    ference = f;
    radius = f / (2 * pi);
    area = pi * radius * radius;
}

void Circle::setArea(double a)
{
    if (a < 0) throw "Area cannot be negative. Try again.";
    area = a;
    radius = sqrt(a / pi);
    ference = 2 * pi * radius;
}

double Circle::getRadius()const {return radius;}

double Circle::getFerence() const {return ference;}

double Circle::getArea() const {return area;}
