#ifndef CIRCLE_H
#define CIRCLE_H

#include <math.h>

class Circle
{
public:
    Circle();
    void setRadius(double r);
    void setFerence(double f);
    void setArea(double a);

    double getRadius() const;
    double getFerence() const;
    double getArea() const;

private:
    double radius;
    double ference;
    double area;
};

#endif // CIRCLE_H
