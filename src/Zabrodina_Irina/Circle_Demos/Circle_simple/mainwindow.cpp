#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->leRadius->setText(QString::number(circle.getRadius()));
    ui->leFerence->setText(QString::number(circle.getFerence()));
    ui->leArea->setText(QString::number(circle.getArea()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbRadius_clicked()
{
    QString radius = ui->leRadius->text();
    try{
        circle.setRadius(radius.toDouble());
        ui->leRadius->setText(QString::number(circle.getRadius()));
        ui->leFerence->setText(QString::number(circle.getFerence()));
        ui->leArea->setText(QString::number(circle.getArea()));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbFerence_clicked()
{
    QString ference = ui->leFerence->text();
    try{
        circle.setFerence(ference.toDouble());
        ui->leRadius->setText(QString::number(circle.getRadius()));
        ui->leFerence->setText(QString::number(circle.getFerence()));
        ui->leArea->setText(QString::number(circle.getArea()));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}

void MainWindow::on_pbArea_clicked()
{
    QString area = ui->leArea->text();
    try{
        circle.setArea(area.toDouble());
        ui->leRadius->setText(QString::number(circle.getRadius()));
        ui->leFerence->setText(QString::number(circle.getFerence()));
        ui->leArea->setText(QString::number(circle.getArea()));
    }
    catch (const char* error)
    {
        QMessageBox::information(this, "Error", error);
    }
}
