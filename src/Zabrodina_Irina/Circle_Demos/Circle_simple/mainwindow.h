#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "C:/Users/Irene/Desktop/Programming/QT/NIIT/Circle_Demos/circle.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbRadius_clicked();
    void on_pbFerence_clicked();
    void on_pbArea_clicked();

private:
    Ui::MainWindow *ui;
    Circle circle;
};

#endif // MAINWINDOW_H
