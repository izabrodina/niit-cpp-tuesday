#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "C:/Users/Irene/Desktop/Programming/QT/NIIT/Circle_Demos/circle.cpp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lePoolRadius->setText("0");
    ui->lePathWidth->setText("0");
    ui->leFencePrice->setText("0.0");
    ui->lePathPrice->setText("0.0");
    ui->leFenceCost->setText("0.0");
    ui->lePathCost->setText("0.0");
    ui->leTotal->setText("0.0");
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_updateButton_clicked()
{
    QString r = ui->lePoolRadius->text();
    QString w = ui->lePathWidth->text();
    QString fp = ui->leFencePrice->text();
    QString pp = ui->lePathPrice->text();
    double pool_radius = r.toDouble();
    double path_width = w.toDouble();
    double fence_price = fp.toDouble();
    double path_price = pp.toDouble();
    double fence_length, path_area, fence_cost, path_cost, total;

    try{
        pool.setRadius(pool_radius);
        if (path_width < 0 || fence_price < 0 || path_price < 0)
            throw "These fields cannot be negative. Please check and try again.";
        path_pool.setRadius(pool.getRadius() + path_width);
        fence_length = path_pool.getFerence();
        fence_cost = fence_price * fence_length;
        ui->leFenceCost->setText(QString::number(fence_cost));

        path_area = path_pool.getArea() - pool.getArea();
        path_cost = path_price * path_area;
        ui->lePathCost->setText(QString::number(path_cost));

        total = fence_cost+ + path_cost;
        ui->leTotal->setText(QString::number(total));
    }
    catch (const char * error){
        QMessageBox::information(this,"Error", error);
    }
}
