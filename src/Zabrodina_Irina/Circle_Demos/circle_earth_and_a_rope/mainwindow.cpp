#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "C:/Users/Irene/Desktop/Programming/QT/NIIT/Circle_Demos/circle.cpp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->leRadius->setText("6378.1");
    QString r = ui->leRadius->text();
    earth.setRadius(r.toDouble());   //convert to double
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_setButton_clicked()
{
    QString added = ui->leAdd->text();
    double addition = added.toDouble();
    addition *= 0.001;
    rope.setFerence(earth.getFerence() + addition);
    double space_earth_rope = rope.getRadius() - earth.getRadius();
    space_earth_rope *= 1000;
    ui->leGap->setText(QString::number(space_earth_rope));
}
